package com.minsait.labs.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	  @JsonProperty("id")
	  private Long id = null;
	  
	  @JsonProperty("name")
	  private String name = null;
	  
	  @JsonProperty("surname")
	  private String surname = null;
	  
	  @JsonProperty("pet")
	  private Pet pet = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

}
