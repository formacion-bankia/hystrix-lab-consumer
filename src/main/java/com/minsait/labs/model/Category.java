package com.minsait.labs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.validation.annotation.Validated;

/**
 * Category
 */
@Validated
public class Category   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("name")
  private String name = null;

  public Category id(Long id) {
    this.id = id;
    return this;
  }
  
    public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Category name(String name) {
    this.name = name;
    return this;
  }
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
