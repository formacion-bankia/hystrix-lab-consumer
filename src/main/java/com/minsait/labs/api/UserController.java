package com.minsait.labs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.minsait.labs.model.User;
import com.minsait.labs.service.UserService;

@Controller
public class UserController {
	
	private UserService userService;
	
	@Autowired
    public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@RequestMapping(value = "/user/{userId}",
    produces = { "application/json", "application/xml" }, 
    method = RequestMethod.GET)
	public ResponseEntity<User> getPetById( @PathVariable("userId") Long userId
	){
		return new ResponseEntity<User>(userService.getUserById(1L),HttpStatus.OK);
	}

}
