package com.minsait.labs.service;

import com.minsait.labs.model.User;

public interface UserService {

	public User getUserById(Long id);
}
