package com.minsait.labs.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.minsait.labs.model.Pet;
import com.minsait.labs.model.User;

@Service
public class UserServiceImpl implements com.minsait.labs.service.UserService {

	@Autowired
	DiscoveryClient discoveryClient;
	
	@Override
	public User getUserById(Long id) {
		User user = new User();
		user.setName("Dummy Client");
		user.setId(id);
		user.setSurname("Dummy Client surname");
		
		Optional<ServiceInstance> serviceInstance = discoveryClient.getInstances("ConsulMsProducer").stream().findFirst();
		String url = serviceInstance.get().getUri().toString().concat("/pet/1");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Pet> response = restTemplate.getForEntity(url, Pet.class);
		user.setPet(response.getBody());
		
		return user;
	}

}
